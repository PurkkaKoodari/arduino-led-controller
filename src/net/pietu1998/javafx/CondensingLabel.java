/*
 * Copyright (c) 2016 VGR, Pietu1998
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package net.pietu1998.javafx;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.*;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * Modified from
 * <a href="https://stackoverflow.com/a/38425597/1938435">https://stackoverflow.com/a/38425597/1938435</a>.
 *
 * @author Pietu1998
 * @author VGR
 */
public class CondensingLabel extends StackPane {

    public CondensingLabel() {
        Label normalLabel = new Label();
        Label condensedLabel = new Label();
        getChildren().addAll(normalLabel, condensedLabel);
        BooleanBinding fullyVisible = Bindings.createBooleanBinding(() ->
                        normalLabel.getWidth() >= normalLabel.prefWidth(normalLabel.getHeight()),
                normalLabel.widthProperty());
        condensed.bind(fullyVisible);
        normalLabel.visibleProperty().bind(fullyVisible);
        condensedLabel.visibleProperty().bind(fullyVisible.not());
        normalLabel.textProperty().bind(normalTextProperty());
        condensedLabel.textProperty().bind(condensedTextProperty());
    }

    public CondensingLabel(String normalText) {
        this(normalText, normalText.substring(0, 1));
    }

    public CondensingLabel(String normalText, String condensedText) {
        this();
        setNormalText(normalText);
        setCondensedText(condensedText);
    }

    private ReadOnlyBooleanWrapper condensed = new ReadOnlyBooleanWrapper(this, "condensed");
    private StringProperty normalText = new SimpleStringProperty(this, "normalText");
    private StringProperty condensedText = new SimpleStringProperty(this, "condensedText");

    public boolean isCondensed() {
        return condensed.get();
    }

    public ReadOnlyBooleanProperty condensedProperty() {
        return condensed.getReadOnlyProperty();
    }

    public String getNormalText() {
        return normalText.get();
    }

    public void setNormalText(String normalText) {
        this.normalText.set(normalText);
    }

    public StringProperty normalTextProperty() {
        return normalText;
    }

    public String getCondensedText() {
        return condensedText.get();
    }

    public void setCondensedText(String condensedText) {
        this.condensedText.set(condensedText);
    }

    public StringProperty condensedTextProperty() {
        return condensedText;
    }
}
