package net.pietu1998.javafx;

import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;

public class DeselectableListCell<T> extends ListCell<T> {
    public DeselectableListCell() {
        addEventFilter(MouseEvent.MOUSE_PRESSED, e -> {
            if (isEmpty()) {
                getListView().getSelectionModel().clearSelection();
                e.consume();
            }
        });
    }
}
