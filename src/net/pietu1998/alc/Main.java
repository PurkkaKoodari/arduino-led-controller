package net.pietu1998.alc;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;
import org.jetbrains.annotations.Nullable;

public class Main extends Application {

    public static final EventType<Event> LOADED = new EventType<>("loaded");

    public static final int VERSION_MAJOR = 1, VERSION_MINOR = 1;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("led_controller.fxml"));
        Parent root = loader.load();
        LedController controller = loader.getController();
        primaryStage.setTitle("Arduino LED Controller");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setMinWidth(800);
        primaryStage.setMinHeight(600);
        root.applyCss();
        controller.cssApplied();

        FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("animation_picker.fxml"));
        Parent dialogRoot = dialogLoader.load();
        AnimationPicker dialogController = dialogLoader.getController();
        Dialog<String> dialog = new Dialog<>();
        dialogController.setDialog(dialog);
        DialogPane dialogPane = new DialogPane() {
            @Override
            @Nullable
            protected Node createButtonBar() {
                return null;
            }
        };
        dialog.setDialogPane(dialogPane);
        dialog.setOnShowing(e -> dialogController.clearSelection());
        dialogPane.setContent(dialogRoot);
        dialogPane.getButtonTypes().add(ButtonType.CLOSE);
        controller.setPickerDialog(dialog, dialogController);

        primaryStage.show();
        primaryStage.setOnCloseRequest(controller::closeRequest);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
