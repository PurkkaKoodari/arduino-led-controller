package net.pietu1998.alc;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.WindowEvent;
import javafx.util.Pair;
import jssc.SerialPortList;
import net.pietu1998.alc.arduino.*;
import net.pietu1998.alc.data.Fade;
import net.pietu1998.alc.plugin.Plugin;
import net.pietu1998.alc.util.ColorUtil;
import net.pietu1998.alc.util.MathUtil;
import net.pietu1998.javafx.DeselectableListCell;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class LedController {
    @FXML
    private TabPane root;
    @FXML
    private ColorChooser directColorPicker, animationColorPicker;
    @FXML
    private Slider directFadeSlider, animationFadeSlider;
    @FXML
    private Label directFadeLabel, animationFadeLabel;
    @FXML
    private Label directColorRect, animationColorRect;
    @FXML
    private Label controllerVersion, protocolVersion, arduinoVersion;
    @FXML
    private Label animationNameLbl;
    @FXML
    private TextField animationNameFld;
    @FXML
    private ListView<Fade> animationList;
    @FXML
    private ComboBox<String> portList;
    @FXML
    private Button scanBtn, connectBtn, disconnectBtn,
            snapBtn, fadeBtn, directStopBtn,
            addSnapBtn, addFadeBtn, dupeBtn, moveUpBtn, moveDownBtn, removeBtn,
            uploadBtn, playBtn, stopBtn,
            addPluginBtn, removePluginBtn;
    @FXML
    private CheckBox autoUpdate, autoplay, loop;
    @FXML
    private StackPane pluginContent;
    @FXML
    private ListView<Plugin> pluginList;

    @Nullable
    private Dialog<String> pickerDialog;

    private final ObservableList<@NotNull String> ports = FXCollections.observableArrayList();
    private final ObservableList<@NotNull Fade> animation = FXCollections.observableArrayList();
    private final StringProperty animationName = new SimpleStringProperty("");
    final ObservableMap<@NotNull String, @NotNull List<@NotNull Fade>> allAnimations = FXCollections.observableHashMap();

    private final SerialController controller = new SerialController();

    private boolean closing = false;

    public void setPickerDialog(@NotNull Dialog<String> pickerDialog, @NotNull AnimationPicker pickerController) {
        this.pickerDialog = pickerDialog;
        pickerController.setController(this);
    }

    void cssApplied() {
        directColorPicker.cssApplied();
        animationColorPicker.cssApplied();
        update();
    }

    @FXML
    @SuppressWarnings("unused")
    public void initialize() {
        controllerVersion.setText(String.format("Controller version: %d.%d", Main.VERSION_MAJOR, Main.VERSION_MINOR));
        protocolVersion.setText(String.format("Protocol version: %d.%d", SerialController.PROTOCOL_VERSION_MAJOR,
                SerialController.PROTOCOL_VERSION_MINOR));
        directFadeSlider.valueProperty().addListener(val ->
                updateFadeLabel(directFadeSlider.getValue(), directFadeLabel));
        animationFadeSlider.valueProperty().addListener(val ->
                updateFadeLabel(animationFadeSlider.getValue(), animationFadeLabel));
        directColorPicker.colorProperty().addListener((val, old, color) -> updateColors(color, directColorRect));
        animationColorPicker.colorProperty().addListener((val, old, color) -> updateColors(color, animationColorRect));
        animationNameLbl.textProperty().bind(animationName);
        animationNameFld.focusedProperty().addListener((val, old, focused) -> {
            if (!focused && animationNameFld.isVisible())
                renameAnimation(null);
        });
        animationList.setCellFactory(param -> new DeselectableListCell<Fade>() {
            @Override
            protected void updateItem(Fade item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null)
                    setText(fadeToString(item));
                else
                    setText(null);
                updateColors(isSelected());
            }

            @Override
            public void updateSelected(boolean selected) {
                super.updateSelected(selected);
                updateColors(selected);
            }

            private void updateColors(boolean selected) {
                if (selected) {
                    if (getItem() != null) {
                        Color textColor = ColorUtil.suitableTextColor(getItem().getColor());
                        setBackground(new Background(new BackgroundFill(textColor, null, null),
                                new BackgroundFill(getItem().getColor(), null, new Insets(4))));
                        setTextFill(textColor);
                    } else {
                        setBackground(new Background(new BackgroundFill(Color.BLUE, null, null)));
                        setTextFill(Color.WHITE);
                    }
                } else if (getItem() != null) {
                    setBackground(new Background(new BackgroundFill(getItem().getColor(), null, null)));
                    setTextFill(ColorUtil.suitableTextColor(getItem().getColor()));
                } else {
                    setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, null, null)));
                }
            }

            private String fadeToString(Fade fade) {
                if (fade.isSnap())
                    return "Snap to " + ColorUtil.toHex(fade.getColor());
                else
                    return String.format("Fade to %s over %.2fs", ColorUtil.toHex(fade.getColor()), fade.getDuration() / 1000.0);
            }
        });
        animationList.setItems(animation);
        animationList.getSelectionModel().selectedIndexProperty().addListener(index -> animationSelectionChanged());
        portList.setItems(ports);
        portList.getSelectionModel().selectedIndexProperty().addListener(index -> updatePortSelection());
        controller.stateProperty().addListener(val -> {
            if (controller.getState() == SerialController.State.NOT_CONNECTED && closing)
                root.getScene().getWindow().hide();
            updatePortSelection();
            updateArduinoVersion();
        });
        controller.connectedProperty().addListener((val, old, connected) -> connectedChanged(connected));
        controller.errorProperty().addListener(val -> updateArduinoVersion());
        loadData();
    }

    //region UI update functions

    private void update() {
        updateFadeLabel(directFadeSlider.getValue(), directFadeLabel);
        updateFadeLabel(animationFadeSlider.getValue(), animationFadeLabel);
        updateColors(directColorPicker.getColor(), directColorRect);
        updateColors(animationColorPicker.getColor(), animationColorRect);
        animationSelectionChanged();
        updatePortList(null);
        updatePortSelection();
        updateArduinoVersion();
        connectedChanged(controller.isConnected());
    }

    public void updatePortList(@Nullable ActionEvent e) {
        String current = portList.getSelectionModel().getSelectedItem();
        ports.setAll(SerialPortList.getPortNames());
        if (ports.contains(current))
            portList.getSelectionModel().select(current);
        else if (!ports.isEmpty())
            portList.getSelectionModel().select(0);
    }

    private void updateArduinoVersion() {
        Pair<Byte, Byte> version = controller.getArduinoVersion();
        if (version != null)
            arduinoVersion.setText(String.format("Arduino version: %d.%d", version.getKey(), version.getValue()));
        else if (controller.getError() != null)
            arduinoVersion.setText("Arduino status: " + controller.getError());
        else
            arduinoVersion.setText("Arduino status: " + controller.getState().uiString);
    }

    private void updateColors(Color backColor, Label colorRect) {
        Background background = new Background(new BackgroundFill(backColor, new CornerRadii(10), null));
        Color textColor = ColorUtil.suitableTextColor(backColor);
        String colorString = ColorUtil.toHex(backColor);
        colorRect.setBackground(background);
        colorRect.setTextFill(textColor);
        colorRect.setText(colorString);
        if (autoUpdate.isSelected())
            snapColor(null);
    }

    private void updateFadeLabel(double fadeValue, Label fadeLabel) {
        int fadeTime = (int) Math.pow(10.0, fadeValue);
        fadeLabel.setText(String.format("Fade time: %.2fs", fadeTime / 1000.0));
    }

    private void animationSelectionChanged() {
        addSnapBtn.setDisable(animation.size() >= 128);
        addFadeBtn.setDisable(animation.size() >= 128);
        dupeBtn.setDisable(animationList.getSelectionModel().isEmpty() || animation.size() >= 128);
        removeBtn.setDisable(animationList.getSelectionModel().isEmpty());
        moveUpBtn.setDisable(animationList.getSelectionModel().isEmpty()
                || animationList.getSelectionModel().getSelectedIndex() == 0);
        moveDownBtn.setDisable(animationList.getSelectionModel().isEmpty()
                || animationList.getSelectionModel().getSelectedIndex() == animation.size() - 1);
    }

    private void updatePortSelection() {
        portList.setDisable(controller.getState() != SerialController.State.NOT_CONNECTED);
        scanBtn.setDisable(controller.getState() != SerialController.State.NOT_CONNECTED);
        connectBtn.setDisable(controller.getState() != SerialController.State.NOT_CONNECTED
                || portList.getSelectionModel().isEmpty());
        disconnectBtn.setDisable(controller.getState() == SerialController.State.NOT_CONNECTED);
    }

    private void connectedChanged(boolean connected) {
        updateDirectControl(null);
        uploadBtn.setDisable(!connected);
        playBtn.setDisable(!connected);
        stopBtn.setDisable(!connected);
        autoplay.setDisable(!connected);
        loop.setDisable(!connected);
        if (connected) {
            autoplay.setSelected(true);
            loop.setSelected(true);
        }
    }

    public void updateDirectControl(@Nullable ActionEvent e) {
        boolean connected = controller.isConnected();
        snapBtn.setDisable(!connected || autoUpdate.isSelected());
        fadeBtn.setDisable(!connected || autoUpdate.isSelected());
        directStopBtn.setDisable(!connected || autoUpdate.isSelected());
    }

    //endregion

    //region Animation list editing functions

    public void addSnap(@Nullable ActionEvent e) {
        animation.add(new Fade(animationColorPicker.getColor(), 0));
        animationList.getSelectionModel().clearSelection();
        saveData();
    }

    public void addFade(@Nullable ActionEvent e) {
        int animationFadeTime = (int) Math.pow(10.0, animationFadeSlider.getValue());
        animation.add(new Fade(animationColorPicker.getColor(), animationFadeTime));
        animationList.getSelectionModel().clearSelection();
        saveData();
    }

    public void dupeStep(ActionEvent e) {
        if (!animationList.getSelectionModel().isEmpty()) {
            int index = animationList.getSelectionModel().getSelectedIndex();
            animation.add(index, animation.get(index));
            animationList.getSelectionModel().clearAndSelect(index + 1);
            saveData();
        }
    }

    public void moveStepUp(@Nullable ActionEvent e) {
        if (!animationList.getSelectionModel().isEmpty()) {
            int index = animationList.getSelectionModel().getSelectedIndex();
            if (index == 0)
                return;
            Fade swap = animation.set(index - 1, animation.get(index));
            animation.set(index, swap);
            animationList.getSelectionModel().clearAndSelect(index - 1);
            saveData();
        }
    }

    public void moveStepDown(@Nullable ActionEvent e) {
        if (!animationList.getSelectionModel().isEmpty()) {
            int index = animationList.getSelectionModel().getSelectedIndex();
            if (index == animation.size() - 1)
                return;
            Fade swap = animation.set(index + 1, animation.get(index));
            animation.set(index, swap);
            animationList.getSelectionModel().clearAndSelect(index + 1);
            saveData();
        }
    }

    public void removeStep(@Nullable ActionEvent e) {
        if (!animationList.getSelectionModel().isEmpty()) {
            int index = animationList.getSelectionModel().getSelectedIndex();
            animationList.getSelectionModel().clearSelection();
            animation.remove(index);
            if (!animation.isEmpty())
                if (index == 0)
                    animationList.getSelectionModel().select(0);
                else
                    animationList.getSelectionModel().select(index - 1);
            saveData();
        }
    }

    //endregion

    //region Data control functions

    @NotNull
    private File getDataFolder() {
        return new File(System.getProperty("user.home"), ".alc");
    }

    @NotNull
    private File getDataFile() {
        return new File(getDataFolder(), "alc.xml");
    }

    private void loadData() {
        assert Platform.isFxApplicationThread();
        allAnimations.clear();
        File dataFile = getDataFile();
        boolean defaultSet = false;
        if (dataFile.exists() && dataFile.canRead()) {
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(dataFile);
                Element root = document.getDocumentElement();
                NodeList animationsElems = root.getElementsByTagName("animations");
                if (animationsElems.getLength() != 1)
                    throw new IOException("invalid data file format");
                NodeList animationElems = ((Element) animationsElems.item(0)).getElementsByTagName("animation");
                for (int i = 0; i < animationElems.getLength(); i++) {
                    Element animationElem = (Element) animationElems.item(i);
                    NodeList nameElems = animationElem.getElementsByTagName("name");
                    if (nameElems.getLength() != 1)
                        throw new IOException("invalid data file format");
                    String name = nameElems.item(0).getTextContent();
                    NodeList fadesElems = animationElem.getElementsByTagName("fades");
                    if (fadesElems.getLength() != 1)
                        throw new IOException("invalid data file format");
                    List<Fade> fades = new ArrayList<>();
                    NodeList fadeElems = ((Element) fadesElems.item(0)).getElementsByTagName("fade");
                    for (int j = 0; j < fadeElems.getLength(); j++) {
                        Element fadeElem = (Element) fadeElems.item(j);
                        int red = MathUtil.clamp(Integer.parseInt(fadeElem.getAttribute("red")), 0, 255);
                        int green = MathUtil.clamp(Integer.parseInt(fadeElem.getAttribute("green")), 0, 255);
                        int blue = MathUtil.clamp(Integer.parseInt(fadeElem.getAttribute("blue")), 0, 255);
                        int duration = Integer.parseInt(fadeElem.getAttribute("duration"));
                        duration = duration == 0 ? 0 : MathUtil.clamp(duration, 100, 60000);
                        fades.add(new Fade(Color.rgb(red, green, blue), duration));
                    }
                    allAnimations.put(name, fades);
                    if (!defaultSet && animationElem.getAttribute("active").equalsIgnoreCase("true")) {
                        animationName.set(name);
                        animation.setAll(fades);
                        defaultSet = true;
                    }
                }
            } catch (SAXException | IOException | ParserConfigurationException | NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to load data file.", ButtonType.OK);
                alert.setHeaderText("Error");
                alert.setTitle("Error");
                alert.show();
            }
        }
        if (!defaultSet)
            createDefaultAnimation();
    }

    private void updateAnimationInMap() {
        @Nullable List<Fade> target = allAnimations.get(animationName.get());
        if (target == null) {
            target = new ArrayList<>(animation.size());
            allAnimations.put(animationName.get(), target);
        } else
            target.clear();
        target.addAll(animation);
    }

    private void saveData() {
        saveData(false);
    }

    void saveData(boolean afterDelete) {
        if (afterDelete) {
            if (allAnimations.isEmpty()) {
                createDefaultAnimation();
                return;
            } else if (!allAnimations.containsKey(animationName.get())) {
                String newChoice = allAnimations.keySet().stream().min(Comparator.naturalOrder())
                        .orElseThrow(AssertionError::new);
                animationName.set(newChoice);
                animation.setAll(allAnimations.get(newChoice));
            }
        }
        updateAnimationInMap();
        FileOutputStream stream = null;
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("data");
            document.appendChild(root);
            Element animationsElem = document.createElement("animations");
            for (Map.Entry<@NotNull String, @NotNull List<@NotNull Fade>> entry : allAnimations.entrySet()) {
                Element animationElem = document.createElement("animation");
                if (animationName.get().equals(entry.getKey()))
                    animationElem.setAttribute("active", "true");
                Element nameElem = document.createElement("name");
                nameElem.setTextContent(entry.getKey());
                animationElem.appendChild(nameElem);
                Element fadesElem = document.createElement("fades");
                for (Fade fade : entry.getValue()) {
                    Element fadeElem = document.createElement("fade");
                    fadeElem.setAttribute("red", Integer.toString((int) (255 * fade.getColor().getRed())));
                    fadeElem.setAttribute("green", Integer.toString((int) (255 * fade.getColor().getGreen())));
                    fadeElem.setAttribute("blue", Integer.toString((int) (255 * fade.getColor().getBlue())));
                    fadeElem.setAttribute("duration", Integer.toString(fade.getDuration()));
                    fadesElem.appendChild(fadeElem);
                }
                animationElem.appendChild(fadesElem);
                animationsElem.appendChild(animationElem);
            }
            root.appendChild(animationsElem);
            File dataFile = getDataFile();
            if (!dataFile.getParentFile().exists() && !dataFile.getParentFile().mkdirs())
                throw new IOException("failed to create parent directory");
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            stream = new FileOutputStream(dataFile);
            transformer.transform(new DOMSource(document), new StreamResult(stream));
        } catch (ParserConfigurationException | TransformerException | IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to write data file.", ButtonType.OK);
            alert.setHeaderText("Error");
            alert.setTitle("Error");
            alert.show();
        } finally {
            try {
                if (stream != null)
                    stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    private String findFreeName(String prefix) {
        String name = prefix;
        int number = 1;
        while (allAnimations.containsKey(name))
            name = prefix + " " + number++;
        return name;
    }

    public void renameKeyPressed(@NotNull KeyEvent e) {
        if (e.getCode() == KeyCode.ESCAPE)
            animationNameFld.setVisible(false);
    }

    public void renameAnimation(@Nullable ActionEvent e) {
        String newName = animationNameFld.getText().trim();
        if (!newName.isEmpty() && !animationName.get().equals(newName)) {
            allAnimations.remove(animationName.get());
            animationName.set(findFreeName(newName));
            saveData();
        }
        animationNameFld.setVisible(false);
    }

    public void editAnimationName(@Nullable MouseEvent e) {
        animationNameFld.setText(animationName.get());
        animationNameFld.setVisible(true);
        animationNameFld.requestFocus();
    }

    public void newAnimation(@Nullable ActionEvent e) {
        createDefaultAnimation();
        editAnimationName(null);
    }

    public void dupeAnimation(@Nullable ActionEvent e) {
        animationName.set(findFreeName(animationName.get() + " copy"));
        editAnimationName(null);
        saveData();
    }

    public void openAnimationList(@Nullable ActionEvent e) {
        updateAnimationInMap();
        if (pickerDialog == null)
            return;
        pickerDialog.showAndWait().ifPresent(chosen -> {
            if (!allAnimations.containsKey(chosen))
                return;
            animationName.set(chosen);
            animation.setAll(allAnimations.get(chosen));
            saveData();
        });
    }

    private void createDefaultAnimation() {
        animation.clear();
        animationName.set(findFreeName("untitled"));
        saveData();
    }

    //endregion

    //region Arduino control functions

    public void connect(@Nullable ActionEvent e) {
        if (!portList.getSelectionModel().isEmpty() && controller.getState() == SerialController.State.NOT_CONNECTED)
            controller.connect(portList.getSelectionModel().getSelectedItem());
    }

    public void disconnect(@Nullable ActionEvent e) {
        if (controller.getState() != SerialController.State.NOT_CONNECTED)
            controller.disconnect();
    }

    public void snapColor(@Nullable ActionEvent e) {
        if (controller.isConnected())
            controller.sendCommand(new ColorCommand(SerialCommand.SNAP, directColorPicker.getColor()));
    }

    public void fadeColor(@Nullable ActionEvent e) {
        if (controller.isConnected())
            controller.sendCommand(new FadeCommand(SerialCommand.FADE, directColorPicker.getColor(),
                    (int) Math.pow(10.0, directFadeSlider.getValue())));
    }

    public void sendAutoplay(@Nullable ActionEvent e) {
        if (controller.isConnected())
            controller.sendCommand(new BooleanCommand(SerialCommand.ANIM_SET_AUTOPLAY, autoplay.isSelected()));
    }

    public void sendLoop(@Nullable ActionEvent e) {
        if (controller.isConnected())
            controller.sendCommand(new BooleanCommand(SerialCommand.ANIM_SET_LOOP, autoplay.isSelected()));
    }

    public void sendPlay(@Nullable ActionEvent e) {
        if (controller.isConnected())
            controller.sendCommand(new SerialCommand(SerialCommand.ANIM_PLAY));
    }

    public void sendStop(@Nullable ActionEvent e) {
        if (controller.isConnected())
            controller.sendCommand(new SerialCommand(SerialCommand.STOP));
    }

    public void uploadAnimation(@Nullable ActionEvent e) {
        if (controller.isConnected()) {
            controller.sendCommand(new SerialCommand(SerialCommand.ANIM_CLEAR));
            for (Fade fade : animation) {
                if (fade.isSnap())
                    controller.sendCommand(new ColorCommand(SerialCommand.ANIM_ADD_SNAP, fade.getColor()));
                else
                    controller.sendCommand(new FadeCommand(SerialCommand.ANIM_ADD_FADE,
                            fade.getColor(), fade.getDuration()));
            }
            controller.sendCommand(new SerialCommand(SerialCommand.ANIM_DONE));
        }
    }

    //endregion

    //region Plugin GUI

    public void addPlugin(@Nullable ActionEvent event) {
        // TODO
    }

    public void removePlugin(@Nullable ActionEvent event) {
        // TODO
    }

    //endregion

    public void closeRequest(@NotNull WindowEvent e) {
        if (controller.getState() != SerialController.State.NOT_CONNECTED) {
            e.consume();
            closing = true;
            updatePortSelection();
            controller.disconnect();
        }
    }
}
