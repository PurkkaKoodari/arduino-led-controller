package net.pietu1998.alc.util;

import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

public final class ColorUtil {
    private ColorUtil() {
    }

    @NotNull
    public static Color suitableTextColor(@NotNull Color background) {
        return getLuma(background) >= 0.5 ? Color.BLACK : Color.WHITE;
    }

    public static double getHue(@NotNull Color color) {
        double r = color.getRed(), g = color.getGreen(), b = color.getBlue();
        double max = MathUtil.max(r, g, b);
        double min = MathUtil.min(r, g, b);
        double chroma = max - min;
        if (chroma == 0)
            return 0;
        if (r == max)
            return ((g - b) / chroma + 6) % 6 * 60;
        if (g == max)
            return (b - r) / chroma * 60;
        return (r - g) / chroma * 60;
    }

    public static double getSaturation(@NotNull Color color) {
        double max = MathUtil.max(color.getRed(), color.getGreen(), color.getBlue());
        double min = MathUtil.min(color.getRed(), color.getGreen(), color.getBlue());
        if (min == max)
            return 0;
        return (max - min) / max;
    }

    public static double getIntensity(@NotNull Color color) {
        return (color.getRed() + color.getGreen() + color.getBlue()) / 3;
    }

    public static double getLightness(@NotNull Color color) {
        double max = MathUtil.max(color.getRed(), color.getGreen(), color.getBlue());
        double min = MathUtil.min(color.getRed(), color.getGreen(), color.getBlue());
        return (max + min) / 2;
    }

    public static double getValue(@NotNull Color color) {
        return MathUtil.max(color.getRed(), color.getGreen(), color.getBlue());
    }

    public static double getChroma(@NotNull Color color) {
        double max = MathUtil.max(color.getRed(), color.getGreen(), color.getBlue());
        double min = MathUtil.min(color.getRed(), color.getGreen(), color.getBlue());
        return max - min;
    }

    public static double getLuma(@NotNull Color color) {
        return color.getRed() * 0.30 + color.getGreen() * 0.59 + color.getBlue() * 0.11;
    }

    @NotNull
    public static String toHex(@NotNull Color color) {
        return String.format("#%02x%02x%02x", (int) (255 * color.getRed()),
                (int) (255 * color.getGreen()), (int) (255 * color.getBlue()));
    }

    @NotNull
    public static Color hsl(double hue, double saturation, double lightness) {
        if (saturation < 0.0 || saturation > 1.0)
            throw new IllegalArgumentException("saturation must be between 0.0 and 1.0");
        if (lightness < 0.0 || lightness > 1.0)
            throw new IllegalArgumentException("lightness must be between 0.0 and 1.0");
        double chroma = (1 - Math.abs(2 * lightness - 1)) * saturation;
        hue = (hue + 360) % 360 / 60;
        double min = MathUtil.clamp(lightness - chroma / 2, 0.0, 1.0);
        double mid = MathUtil.clamp(min + chroma * (1 - Math.abs(hue % 2 - 1)), 0.0, 1.0);
        double max = MathUtil.clamp(min + chroma, 0.0, 1.0);
        //System.err.printf("%.3f %.3f %.3f -> %.3f %.3f %.3f\n", hue, saturation, lightness, min, mid, max);
        if (hue < 1)
            return Color.color(max, mid, min);
        else if (hue < 2)
            return Color.color(mid, max, min);
        else if (hue < 3)
            return Color.color(min, max, mid);
        else if (hue < 4)
            return Color.color(min, mid, max);
        else if (hue < 5)
            return Color.color(mid, min, max);
        else
            return Color.color(max, min, mid);
    }
}
