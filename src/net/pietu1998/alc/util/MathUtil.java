package net.pietu1998.alc.util;

public final class MathUtil {
    private MathUtil() {
    }

    public static int max(int... vals) {
        if (vals.length == 0)
            throw new IllegalArgumentException("must have at least one argument");
        int max = vals[0];
        for (int i = 1; i < vals.length; i++)
            max = Math.max(max, vals[i]);
        return max;
    }

    public static double max(double... vals) {
        if (vals.length == 0)
            throw new IllegalArgumentException("must have at least one argument");
        double max = vals[0];
        for (int i = 1; i < vals.length; i++)
            max = Math.max(max, vals[i]);
        return max;
    }

    public static int min(int... vals) {
        if (vals.length == 0)
            throw new IllegalArgumentException("must have at least one argument");
        int min = vals[0];
        for (int i = 1; i < vals.length; i++)
            min = Math.min(min, vals[i]);
        return min;
    }

    public static double min(double... vals) {
        if (vals.length == 0)
            throw new IllegalArgumentException("must have at least one argument");
        double min = vals[0];
        for (int i = 1; i < vals.length; i++)
            min = Math.min(min, vals[i]);
        return min;
    }

    public static int clamp(int value, int min, int max) {
        return value < min ? min : value > max ? max : value;
    }

    public static double clamp(double value, double min, double max) {
        return value < min ? min : value > max ? max : value;
    }
}
