package net.pietu1998.alc;

import javafx.beans.InvalidationListener;
import javafx.collections.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import net.pietu1998.javafx.DeselectableListCell;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class AnimationPicker {
    @FXML
    private ListView<String> animationsList;
    @FXML
    private Button openBtn, removeBtn, exportBtn;

    @NotNull
    private final ObservableList<String> animations = FXCollections.observableArrayList();
    @Nullable
    private LedController mainController;

    @Nullable
    private Dialog<String> dialog;

    @FXML
    @SuppressWarnings("unused")
    public void initialize() {
        MultipleSelectionModel<String> model = animationsList.getSelectionModel();
        animationsList.setCellFactory(param -> {
            DeselectableListCell<String> cell = new DeselectableListCell<>();
            cell.textProperty().bind(cell.itemProperty());
            return cell;
        });
        animationsList.setItems(animations.sorted());
        model.setSelectionMode(SelectionMode.MULTIPLE);
        model.getSelectedItems().addListener((ListChangeListener<String>) change -> {
            boolean empty = model.isEmpty();
            openBtn.setDisable(empty);
            removeBtn.setDisable(empty);
            exportBtn.setDisable(empty);
        });
    }

    public void listClick(@NotNull MouseEvent e) {
        if (e.getClickCount() == 2) {
            openAnimation(null);
        }
    }

    public void setDialog(@NotNull Dialog<String> dialog) {
        this.dialog = dialog;
        dialog.setResultConverter(new Callback<ButtonType, String>() {
            @Override
            @Nullable
            public String call(ButtonType param) {
                return null;
            }
        });
    }

    public void setController(@NotNull LedController controller) {
        mainController = controller;
        controller.allAnimations.addListener((InvalidationListener) change ->
                animations.setAll(controller.allAnimations.keySet()));
        animations.setAll(controller.allAnimations.keySet());
    }

    @Nullable
    public String getSelectedAnimation() {
        return animationsList != null ? animationsList.getSelectionModel().getSelectedItem() : null;
    }

    public void openAnimation(@Nullable ActionEvent e) {
        String selected = getSelectedAnimation();
        if (dialog != null && selected != null)
            setResultAndClose(selected);
    }

    public void removeAnimations(@Nullable ActionEvent e) {
        MultipleSelectionModel<String> model = animationsList.getSelectionModel();
        int selections = model.getSelectedIndices().size();
        String deleteSpec = selections == 1 ? "\"" + model.getSelectedItem() + "\"" : selections + " animations";
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete " + deleteSpec + "?",
                ButtonType.YES, ButtonType.NO);
        alert.setHeaderText("Delete?");
        alert.setTitle("Delete?");
        if (mainController != null && alert.showAndWait().filter(btn -> btn == ButtonType.YES).isPresent()) {
            for (String key : model.getSelectedItems().toArray(new String[0]))
                mainController.allAnimations.remove(key);
            mainController.saveData(true);
        }
    }

    public void closeDialog(@Nullable ActionEvent e) {
        if (dialog != null)
            setResultAndClose(null);
    }

    private void setResultAndClose(@Nullable String result) {
        if (dialog != null) {
            @Nullable String old = dialog.getResult();
            dialog.setResult(result);
            if (old == result)
                dialog.close();
        }
    }

    void clearSelection() {
        animationsList.getSelectionModel().clearSelection();
    }
}
