package net.pietu1998.alc;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Slider;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.*;
import net.pietu1998.alc.util.ColorUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class ColorChooser extends GridPane {
    @FXML
    private Slider redSlider, greenSlider, blueSlider, hueSlider, saturationSlider, lightnessSlider;

    public ColorChooser() {
        this(Color.BLACK);
    }

    public ColorChooser(@NotNull Color initialColor) {
        setColor(initialColor);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("color_chooser.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private IntegerProperty red, green, blue;
    private DoubleProperty hue, saturation, lightness;
    private ObjectProperty<Color> color;

    private ChangeListener<Color> colorListener;
    private ChangeListener<Number> rgbListener, hslListener;

    public int getRed() {
        return red == null ? 0 : red.get();
    }

    public void setRed(int red) {
        if (red < 0 || red > 255)
            throw new IllegalArgumentException("red must be between 0 and 255");
        redProperty().set(red);
    }

    @NotNull
    public IntegerProperty redProperty() {
        if (red == null)
            red = new SimpleIntegerProperty(this, "red", 0);
        return red;
    }

    public int getGreen() {
        return green == null ? 0 : green.get();
    }

    public void setGreen(int green) {
        if (green < 0 || green > 255)
            throw new IllegalArgumentException("green must be between 0 and 255");
        greenProperty().set(green);
    }

    @NotNull
    public IntegerProperty greenProperty() {
        if (green == null)
            green = new SimpleIntegerProperty(this, "green", 0);
        return green;
    }

    public int getBlue() {
        return blue == null ? 0 : blue.get();
    }

    public void setBlue(int blue) {
        if (blue < 0 || blue > 255)
            throw new IllegalArgumentException("blue must be between 0 and 255");
        blueProperty().set(blue);
    }

    @NotNull
    public IntegerProperty blueProperty() {
        if (blue == null)
            blue = new SimpleIntegerProperty(this, "blue", 0);
        return blue;
    }

    public double getHue() {
        return hue == null ? 0 : hue.get();
    }

    public void setHue(double hue) {
        hueProperty().set((hue + 360) % 360);
    }

    @NotNull
    public DoubleProperty hueProperty() {
        if (hue == null)
            hue = new SimpleDoubleProperty(this, "hue", 0.0);
        return hue;
    }

    public double getSaturation() {
        return saturation == null ? 0.0 : saturation.get();
    }

    public void setSaturation(double saturation) {
        if (saturation < 0.0 || saturation > 1.0)
            throw new IllegalArgumentException("saturation must be between 0.0 and 1.0");
        saturationProperty().set(saturation);
    }

    @NotNull
    public DoubleProperty saturationProperty() {
        if (saturation == null)
            saturation = new SimpleDoubleProperty(this, "saturation", 0);
        return saturation;
    }

    public double getLightness() {
        return lightness == null ? 0 : lightness.get();
    }

    public void setLightness(double lightness) {
        if (lightness < 0.0 || lightness > 1.0)
            throw new IllegalArgumentException("lightness must be between 0.0 and 1.0");
        lightnessProperty().set(lightness);
    }

    @NotNull
    public DoubleProperty lightnessProperty() {
        if (lightness == null)
            lightness = new SimpleDoubleProperty(this, "lightness", 0);
        return lightness;
    }

    @NotNull
    public Color getColor() {
        return color == null ? Color.BLACK : color.get();
    }

    public void setColor(@NotNull Color color) {
        if (!colorProperty().isBound())
            colorProperty().set(color);
    }

    @NotNull
    public ObjectProperty<Color> colorProperty() {
        if (color == null)
            color = new SimpleObjectProperty<>(this, "color", Color.rgb(getRed(), getGreen(), getBlue()));
        return color;
    }

    void cssApplied() {
        updateColor(getColor());
    }

    @FXML
    public void initialize() {
        redProperty().bindBidirectional(redSlider.valueProperty());
        greenProperty().bindBidirectional(greenSlider.valueProperty());
        blueProperty().bindBidirectional(blueSlider.valueProperty());
        hueProperty().bindBidirectional(hueSlider.valueProperty());
        saturationProperty().bindBidirectional(saturationSlider.valueProperty());
        lightnessProperty().bindBidirectional(lightnessSlider.valueProperty());
        rgbListener = (val, old, clr) -> updateColorRGB();
        hslListener = (val, old, clr) -> updateColorHSL();
        redProperty().addListener(rgbListener);
        greenProperty().addListener(rgbListener);
        blueProperty().addListener(rgbListener);
        hueProperty().addListener(hslListener);
        saturationProperty().addListener(hslListener);
        lightnessProperty().addListener(hslListener);
        colorProperty().addListener(colorListener = (val, old, clr) -> {
            setRed((int) (255 * clr.getRed()));
            setGreen((int) (255 * clr.getGreen()));
            setBlue((int) (255 * clr.getBlue()));
            setHue(clr.getHue());
            setSaturation(clr.getSaturation());
            setLightness(ColorUtil.getLightness(clr));
        });
    }

    private <T extends Number> void assignSafely(Property<T> prop, ChangeListener<? super T> listener, T value) {
        prop.removeListener(listener);
        prop.setValue(value);
        prop.addListener(listener);
    }

    private void updateColorRGB() {
        Color clr = Color.rgb(getRed(), getGreen(), getBlue());
        updateColor(clr);
        assignSafely(hueProperty(), hslListener, clr.getHue());
        assignSafely(saturationProperty(), hslListener, clr.getSaturation());
        assignSafely(lightnessProperty(), hslListener, ColorUtil.getLightness(clr));
    }

    private void updateColorHSL() {
        Color clr = ColorUtil.hsl(getHue(), getSaturation(), getLightness());
        updateColor(clr);
        assignSafely(redProperty(), rgbListener, (int) (255 * clr.getRed()));
        assignSafely(greenProperty(), rgbListener, (int) (255 * clr.getGreen()));
        assignSafely(blueProperty(), rgbListener, (int) (255 * clr.getBlue()));
    }

    private void updateColor(Color color) {
        colorProperty().removeListener(colorListener);
        setColor(color);
        colorProperty().addListener(colorListener);
        // Red slider gradient
        Stop start = new Stop(0, Color.rgb(0, getGreen(), getBlue()));
        Stop end = new Stop(1, Color.rgb(255, getGreen(), getBlue()));
        Paint gradient = new LinearGradient(0, 1, 0, 0, true, CycleMethod.NO_CYCLE, start, end);
        StackPane track = (StackPane) redSlider.lookup(".track");
        if (track != null)
            track.setBackground(new Background(new BackgroundFill(gradient, null, null)));
        // Green slider gradient
        start = new Stop(0, Color.rgb(getRed(), 0, getBlue()));
        end = new Stop(1, Color.rgb(getRed(), 255, getBlue()));
        gradient = new LinearGradient(0, 1, 0, 0, true, CycleMethod.NO_CYCLE, start, end);
        track = (StackPane) greenSlider.lookup(".track");
        if (track != null)
            track.setBackground(new Background(new BackgroundFill(gradient, null, null)));
        // Blue slider gradient
        start = new Stop(0, Color.rgb(getRed(), getGreen(), 0));
        end = new Stop(1, Color.rgb(getRed(), getGreen(), 255));
        gradient = new LinearGradient(0, 1, 0, 0, true, CycleMethod.NO_CYCLE, start, end);
        track = (StackPane) blueSlider.lookup(".track");
        if (track != null)
            track.setBackground(new Background(new BackgroundFill(gradient, null, null)));
        // Saturation slider gradient
        start = new Stop(0, ColorUtil.hsl(getHue(), 0.0, 0.5));
        end = new Stop(1, ColorUtil.hsl(getHue(), 1.0, 0.5));
        gradient = new LinearGradient(0, 1, 0, 0, true, CycleMethod.NO_CYCLE, start, end);
        track = (StackPane) saturationSlider.lookup(".track");
        if (track != null)
            track.setBackground(new Background(new BackgroundFill(gradient, null, null)));
        // Lightness slider gradient
        start = new Stop(0, ColorUtil.hsl(getHue(), getSaturation(), 0.0));
        Stop mid = new Stop(0.5, ColorUtil.hsl(getHue(), getSaturation(), 0.5));
        end = new Stop(1, ColorUtil.hsl(getHue(), getSaturation(), 1.0));
        gradient = new LinearGradient(0, 1, 0, 0, true, CycleMethod.NO_CYCLE, start, mid, end);
        track = (StackPane) lightnessSlider.lookup(".track");
        if (track != null)
            track.setBackground(new Background(new BackgroundFill(gradient, null, null)));
    }
}
