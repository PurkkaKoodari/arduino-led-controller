package net.pietu1998.alc.arduino;

import javafx.scene.paint.Color;
import jssc.SerialPort;
import jssc.SerialPortException;
import org.jetbrains.annotations.NotNull;

public class ColorCommand extends SerialCommand {

    @NotNull
    private final Color color;

    public ColorCommand(int id, @NotNull Color color) {
        super(id);
        this.color = color;
    }

    @NotNull
    public Color getColor() {
        return color;
    }

    @Override
    public void send(@NotNull SerialPort port) throws SerialPortException {
        super.send(port);
        port.writeByte((byte) (255 * color.getRed()));
        port.writeByte((byte) (255 * color.getGreen()));
        port.writeByte((byte) (255 * color.getBlue()));
    }

}
