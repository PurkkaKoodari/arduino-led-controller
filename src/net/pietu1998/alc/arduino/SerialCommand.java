package net.pietu1998.alc.arduino;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

public class SerialCommand {
    private final byte id;

    public static final byte KEEP_ALIVE = 0x00;
    public static final byte SNAP = 0x10;
    public static final byte FADE = 0x11;
    public static final byte STOP = 0x12;
    public static final byte ANIM_CLEAR = 0x20;
    public static final byte ANIM_DONE = 0x21;
    public static final byte ANIM_PLAY = 0x22;
    public static final byte ANIM_ADD_SNAP = 0x30;
    public static final byte ANIM_ADD_FADE = 0x31;
    public static final byte ANIM_SET_AUTOPLAY = 0x40;
    public static final byte ANIM_SET_LOOP = 0x41;

    public SerialCommand(int id) {
        if (id < 0 || id > 255)
            throw new IllegalArgumentException("id must be between 0 and 255");
        this.id = (byte) id;
    }

    public byte getId() {
        return id;
    }

    /**
     * Sends this command to the specified serial port.
     *
     * @param port the serial port to send to.
     * @throws SerialPortException if an I/O error occurs.
     */
    public void send(@NotNull SerialPort port) throws SerialPortException {
        port.writeByte(getId());
    }

    public byte[] receive(@NotNull SerialPort port, int timeout)
            throws SerialPortException, SerialPortTimeoutException {
        return port.readBytes(getResponseLength(), timeout);
    }

    public int getResponseLength() {
        return 1;
    }
}
