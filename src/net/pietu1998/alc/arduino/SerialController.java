package net.pietu1998.alc.arduino;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.util.Pair;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;

public class SerialController {
    private static final byte[] MAGIC = "ALCP".getBytes();

    public static final int PROTOCOL_VERSION_MAJOR = 1, PROTOCOL_VERSION_MINOR = 0;

    @Nullable
    private CommunicatorThread thread;

    @NotNull
    private LinkedBlockingQueue<SerialCommand> commandQueue = new LinkedBlockingQueue<>();

    @NotNull
    private ReadOnlyBooleanWrapper connected = new ReadOnlyBooleanWrapper(false);
    @NotNull
    private ReadOnlyObjectWrapper<State> state = new ReadOnlyObjectWrapper<>(State.NOT_CONNECTED);
    @NotNull
    private ReadOnlyStringWrapper error = new ReadOnlyStringWrapper(null);
    @NotNull
    private ReadOnlyObjectWrapper<Pair<Byte, Byte>> arduinoVersion = new ReadOnlyObjectWrapper<>(null);

    {
        connected.bind(state.isEqualTo(State.CONNECTED));
    }

    public boolean isConnected() {
        return connected.get();
    }

    @NotNull
    public ReadOnlyBooleanProperty connectedProperty() {
        return connected.getReadOnlyProperty();
    }

    @NotNull
    public State getState() {
        return state.get();
    }

    @NotNull
    public ReadOnlyObjectProperty<State> stateProperty() {
        return state.getReadOnlyProperty();
    }

    @Nullable
    public String getError() {
        return error.get();
    }

    @NotNull
    public ReadOnlyStringProperty errorProperty() {
        return error.getReadOnlyProperty();
    }

    @Nullable
    public Pair<Byte, Byte> getArduinoVersion() {
        return arduinoVersion.get();
    }

    @NotNull
    public ReadOnlyObjectProperty<Pair<Byte, Byte>> arduinoVersionProperty() {
        return arduinoVersion.getReadOnlyProperty();
    }

    public void connect(String portName) {
        if (getState() != State.NOT_CONNECTED)
            throw new IllegalStateException("already connecting or connected");
        error.set(null);
        state.set(State.CONNECTING);
        thread = new CommunicatorThread(portName);
        thread.start();
    }

    public void disconnect() {
        if (getState() == State.NOT_CONNECTED)
            throw new IllegalStateException("already disconnected");
        if (thread != null)
            thread.interrupt();
    }

    public void sendCommand(@NotNull SerialCommand command) {
        if (!isConnected())
            throw new IllegalStateException("not connected");
        commandQueue.add(command);
    }

    private void disconnect(String error) {
        this.error.set(error);
        disconnect();
    }

    private void disconnected() {
        arduinoVersion.set(null);
        state.set(State.NOT_CONNECTED);
    }

    private void disconnected(String error) {
        this.error.set(error);
        disconnected();
    }

    private void handleResponse(SerialCommand command, byte[] response) {
        switch (command.getId()) {
        case SerialCommand.KEEP_ALIVE:
            if (response[0] != 0)
                disconnect("keep-alive ping failed");
            break;
        default:
            break;
        }
    }

    private class CommunicatorThread extends Thread {
        @NotNull
        private SerialPort port;

        public CommunicatorThread(@NotNull String portName) {
            super(portName + " communicator thread");
            setDaemon(true);
            this.port = new SerialPort(portName);
        }

        @Override
        public void run() {
            String error = null;
            try {
                port.openPort();
                port.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8,
                        SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            } catch (SerialPortException e) {
                error = String.format("opening port %s failed: %s", e.getPortName(), e.getExceptionType());
            }
            if (error == null) {
                try {
                    byte[] handshake = port.readBytes(8, 10000);
                    if (!Arrays.equals(Arrays.copyOfRange(handshake, 0, 4), MAGIC)) {
                        error = "unknown device: unrecognized handshake";
                    } else if (handshake[4] != PROTOCOL_VERSION_MAJOR || handshake[5] != PROTOCOL_VERSION_MINOR) {
                        error = "protocol version mismatch: controller: " + PROTOCOL_VERSION_MAJOR + "." +
                                PROTOCOL_VERSION_MINOR + ", arduino: " + handshake[4] + "." + handshake[5];
                    } else {
                        Platform.runLater(() -> {
                            arduinoVersion.set(new Pair<>(handshake[6], handshake[7]));
                            state.set(SerialController.State.CONNECTED);
                        });
                        while (!isInterrupted()) {
                            final SerialCommand command = commandQueue.take();
                            command.send(port);
                            final byte[] response = command.receive(port, 1000);
                            Platform.runLater(() -> handleResponse(command, response));
                        }
                    }
                } catch (SerialPortException e) {
                    error = String.format("IO error: %s", e.getExceptionType());
                } catch (SerialPortTimeoutException e) {
                    error = "read timed out";
                } catch (InterruptedException e) {
                    interrupt();
                }
            }
            try {
                if (port.isOpened())
                    port.closePort();
            } catch (SerialPortException e) {
                if (error == null)
                    error = String.format("closing port %s failed: %s", e.getPortName(), e.getExceptionType());
            }
            commandQueue.clear();
            if (error != null) {
                String finalError = error;
                Platform.runLater(() -> disconnected(finalError));
            } else {
                Platform.runLater(SerialController.this::disconnected);
            }
        }
    }

    public enum State {
        NOT_CONNECTED("not connected"), CONNECTING("connecting"), CONNECTED("connected");

        public final String uiString;

        State(String uiString) {
            this.uiString = uiString;
        }
    }
}
