package net.pietu1998.alc.arduino;

import jssc.SerialPort;
import jssc.SerialPortException;
import org.jetbrains.annotations.NotNull;

public class BooleanCommand extends SerialCommand {

    private final boolean value;

    public BooleanCommand(int id, boolean value) {
        super(id);
        this.value = value;
    }

    public boolean isValue() {
        return value;
    }

    @Override
    public void send(@NotNull SerialPort port) throws SerialPortException {
        super.send(port);
        port.writeByte((byte) (value ? 1 : 0));
    }
}
