package net.pietu1998.alc.arduino;

import javafx.scene.paint.Color;
import jssc.SerialPort;
import jssc.SerialPortException;
import org.jetbrains.annotations.NotNull;

public class FadeCommand extends ColorCommand {
    private final int duration;

    public FadeCommand(byte id, Color color, int duration) {
        super(id, color);
        if (duration < 0 || duration > 65535)
            throw new IllegalArgumentException("duration must be between 0 and 65535");
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public void send(@NotNull SerialPort port) throws SerialPortException {
        super.send(port);
        port.writeByte((byte) (duration & 0xFF));
        port.writeByte((byte) ((duration >> 8) & 0xFF));
    }

}
