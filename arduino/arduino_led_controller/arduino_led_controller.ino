#define MAJOR_VERSION 1
#define MINOR_VERSION 1
#define PROTOCOL_MAJOR_VERSION 1
#define PROTOCOL_MINOR_VERSION 0

#define MAX_ANIMATION_LENGTH 128

const byte CMD_KEEP_ALIVE         = 0x00;
const byte CMD_SNAP               = 0x10;
const byte CMD_FADE               = 0x11;
const byte CMD_STOP               = 0x12;
const byte CMD_ANIM_CLEAR         = 0x20;
const byte CMD_ANIM_DONE          = 0x21;
const byte CMD_ANIM_PLAY          = 0x22;
const byte CMD_ANIM_ADD_SNAP      = 0x30;
const byte CMD_ANIM_ADD_FADE      = 0x31;
const byte CMD_ANIM_SET_AUTOPLAY  = 0x40;
const byte CMD_ANIM_SET_LOOP      = 0x41;

const byte RESP_SUCCESS = 0;
const byte RESP_OVERFLOW = 1;
const byte RESP_EMPTY = 2;
const byte RESP_NOT_PLAYING = 3;

const byte HANDSHAKE[] = { 'A', 'L', 'C', 'P', PROTOCOL_MAJOR_VERSION, PROTOCOL_MINOR_VERSION, MAJOR_VERSION, MINOR_VERSION };

const byte MODE_CONSTANT = 0;
const byte MODE_FADING = 1;
const byte MODE_ANIMATING = 2;

const byte FLAG_LOOP = 1;
const byte FLAG_AUTOPLAY = 2;

const byte PIN_RED = 5;
const byte PIN_GREEN = 3;
const byte PIN_BLUE = 6;

struct color {
  byte red, green, blue;
};
struct snap_data {
  color clr;
};
struct fade_data {
  color clr;
  unsigned short duration;
};

void setup() {
  pinMode(PIN_RED, OUTPUT);
  pinMode(PIN_GREEN, OUTPUT);
  pinMode(PIN_BLUE, OUTPUT);
  Serial.begin(115200, SERIAL_8N1);
  while (!Serial);
  Serial.write(HANDSHAKE, sizeof(HANDSHAKE));
}

byte cmd;
byte buf[16];
byte mode = MODE_CONSTANT;
byte flags = FLAG_LOOP | FLAG_AUTOPLAY;
unsigned long fadeStart;
int fadeDuration;
color last, target, start;
fade_data animation[MAX_ANIMATION_LENGTH];
byte animationLength = 0;
byte animationIndex = 0;

void playAnimation(bool initial) {
  mode = MODE_FADING | MODE_ANIMATING;
  start = target;
  animationIndex++;
  if (initial || animationIndex >= animationLength)
    animationIndex = 0;
  target = animation[animationIndex].clr;
  fadeDuration = animation[animationIndex].duration;
  fadeStart = millis();
}

void loop() {
  if (Serial.available() > 0) {
    cmd = (byte) Serial.read();
    switch (cmd) {
      case CMD_KEEP_ALIVE:
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_SNAP:
        Serial.readBytes(buf, sizeof(snap_data));
        mode = MODE_CONSTANT;
        target = ((snap_data*) buf)->clr;
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_FADE:
        Serial.readBytes(buf, sizeof(fade_data));
        mode = MODE_FADING;
        start = last;
        target = ((fade_data*) buf)->clr;
        fadeStart = millis();
        fadeDuration = ((fade_data*) buf)->duration;
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_ANIM_CLEAR:
        mode = MODE_CONSTANT;
        target = last;
        animationIndex = 0;
        animationLength = 0;
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_ANIM_DONE:
        if (animationLength && flags & FLAG_AUTOPLAY) {
          playAnimation(true);
        }
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_ANIM_ADD_SNAP:
        Serial.readBytes(buf, sizeof(snap_data));
        if (animationLength >= MAX_ANIMATION_LENGTH) {
          Serial.write(RESP_OVERFLOW);
        } else {
          animation[animationLength].clr = ((snap_data*)buf)->clr;
          animation[animationLength].duration = 0;
          animationLength++;
          Serial.write(RESP_SUCCESS);
        }
        break;
      case CMD_ANIM_ADD_FADE:
        Serial.readBytes(buf, sizeof(fade_data));
        if (animationLength >= MAX_ANIMATION_LENGTH) {
          Serial.write(RESP_OVERFLOW);
        } else {
          animation[animationLength] = *(fade_data*)buf;
          animationLength++;
          Serial.write(RESP_SUCCESS);
        }
        break;
      case CMD_ANIM_SET_AUTOPLAY:
        Serial.readBytes(buf, 1);
        if (buf[0])
          flags |= FLAG_AUTOPLAY;
        else
          flags &= ~FLAG_AUTOPLAY;
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_ANIM_SET_LOOP:
        Serial.readBytes(buf, 1);
        if (buf[0])
          flags |= FLAG_LOOP;
        else
          flags &= ~FLAG_LOOP;
        Serial.write(RESP_SUCCESS);
        break;
      case CMD_ANIM_PLAY:
        if (animationLength) {
          playAnimation(false);
          Serial.write(RESP_SUCCESS);
        } else {
          Serial.write(RESP_EMPTY);
        }
        break;
      case CMD_STOP:
        if (mode & MODE_FADING) {
          mode = MODE_CONSTANT;
          target = last;
          Serial.write(RESP_SUCCESS);
        } else {
          Serial.write(RESP_NOT_PLAYING);
        }
        break;
      default:
        break;
    }
  }
  unsigned long now = millis();
  if (mode & MODE_ANIMATING) {
    if (flags & FLAG_AUTOPLAY) {
      int skipCount = 0;
      while (skipCount <= animationLength && now - fadeStart >= fadeDuration) {
        start = target;
        fadeStart += fadeDuration;
        animationIndex++;
        if (animationIndex >= animationLength) {
          if (flags & FLAG_LOOP) {
            animationIndex = 0;
          } else {
            mode = MODE_CONSTANT;
            goto render;
          }
        }
        target = animation[animationIndex].clr;
        fadeDuration = animation[animationIndex].duration;
        mode = MODE_ANIMATING | MODE_FADING;
        skipCount++;
      }
    } else if (now - fadeStart >= fadeDuration) {
      mode = MODE_ANIMATING;
    }
  }
render:
  if (mode & MODE_FADING) {
    int fadePos = now - fadeStart;
    if (fadePos >= fadeDuration) {
      mode = MODE_CONSTANT;
      last = target;
    } else {
      float fadePosRel = (float)fadePos / (float)fadeDuration;
      last.red = (byte)(start.red + (target.red - start.red) * fadePosRel);
      last.green = (byte)(start.green + (target.green - start.green) * fadePosRel);
      last.blue = (byte)(start.blue + (target.blue - start.blue) * fadePosRel);
    }
  } else {
    last = target;
  }
  analogWrite(PIN_RED, last.red);
  analogWrite(PIN_GREEN, last.green);
  analogWrite(PIN_BLUE, last.blue);
}
