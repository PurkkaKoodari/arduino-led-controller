package net.pietu1998.alc.plugin;

import net.pietu1998.alc.data.Fade;

import java.util.List;

/**
 * Represents the controller using the plugin.
 */
public interface Controller {

    /**
     * Uploads an animation to the controller. This should not replace the animation in the GUI.
     *
     * @param animation the animation to send
     */
    public void uploadAnimation(List<Fade> animation);

    /**
     * Starts a fade to the next step on the controller.
     */
    public void playAnimation();

    /**
     * Stops any ongoing fade on the controller.
     */
    public void stopAnimation();

    /**
     * <p>
     * Sets the autoplay mode on or off on the controller.
     * </p>
     * <p>
     * A value of {@code true} causes the animation to automatically advance to the next step when an animation is
     * uploaded or a fade is completed. A value of {@code false} causes the animation to automatically pause when an
     * animation is uploaded or a fade is completed.
     * </p>
     *
     * @param autoplay whether or not to enable autoplay
     */
    public void setAutoplay(boolean autoplay);

    /**
     * <p>
     * Sets the loop mode on or off on the controller.
     * </p>
     * <p>
     * A value of {@code true} causes the animation to automatically advance to the first step when the last fade is
     * completed. A value of {@code false} causes the animation to pause at the last step when the last fade is
     * completed.
     * </p>
     *
     * @param loop whether or not to enable loop
     */
    public void setLoop(boolean loop);

    /**
     * Indicates that the plugin has encountered an error.
     *
     * @param plugin the plugin encountering the error
     * @param string the error message
     */
    public void pluginError(Plugin plugin, String string);

    /**
     * Indicates that the plugin has encountered an error.
     *
     * @param plugin    the plugin encountering the error
     * @param string    the error message
     * @param throwable the exception thrown
     */
    public void pluginError(Plugin plugin, String string, Throwable throwable);

}
