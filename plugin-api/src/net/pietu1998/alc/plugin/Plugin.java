package net.pietu1998.alc.plugin;

import javafx.scene.Node;

import java.util.concurrent.Future;

/**
 * <p>
 * Represents a plugin.
 * </p>
 * <p>
 * An implementing class must have a default constructor with no arguments. The application will instantiate one
 * instance of the class in order to load the plugin.
 * </p>
 */
public interface Plugin {

    /**
     * Gets the plugin's name. This method should return a constant value.
     *
     * @return the plugin's name
     */
    public String getName();

    /**
     * Gets whether the user is disabled from controlling the lighting while this plugin is active. This
     * method should return a constant value.
     *
     * @return {@code true} if the user shouldn't be allowed to control the lighting while this plugin is active,
     * {@code false} otherwise
     */
    public boolean disableUserControls();

    /**
     * Gets the configuration interface for this plugin. This method should return the same object (or {@code null})
     * for the same instance of this plugin.
     *
     * @return the interface
     */
    public Node getInterface();

    /**
     * <p>
     * Initializes this plugin. This method is only called once in the plugin's lifetime and before any other method.
     * </p>
     * <p>
     * The plugin should start any background operations when {@code initialize()} is called.
     * </p>
     */
    public void initialize(Controller controller);

    /**
     * <p>
     * Indicates to the plugin that it has become the active plugin.
     * </p>
     * <p>
     * The plugin may start using the methods of the controller after {@code start()} is called.
     * </p>
     */
    public void start();

    /**
     * <p>
     * Indicates to the plugin that it is no longer the active plugin.
     * </p>
     * <p>
     * The plugin should not use any methods of the controller after {@code stop()} is called.
     * </p>
     */
    public void stop();

    /**
     * <p>
     * Shuts this plugin down. This method is only called once in the plugin's lifetime and no methods will be called
     * after this one.
     * </p>
     * <p>
     * The plugin should gracefully end all background operations and return a {@link Future} that
     * completes when the plugin has shut down and the controller can exit.
     * </p>
     *
     * @return a {@link Future} that completes when the plugin has shut down.
     */
    public Future<Void> shutdown();

}
