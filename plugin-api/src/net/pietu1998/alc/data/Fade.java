package net.pietu1998.alc.data;

import javafx.scene.paint.Color;

/**
 * Represents a fade to a color in an animation.
 */
public class Fade {
    private final Color color;
    private final int duration;

    /**
     * Constructs a fade.
     *
     * @param color    the color to fade to. Must not be {@code null}.
     * @param duration the duration of the fade, in milliseconds. Must not be negative.
     */
    public Fade(Color color, int duration) {
        if (color == null)
            throw new NullPointerException("color must not be null");
        if (duration < 0)
            throw new IllegalArgumentException("duration must not be negative");
        this.color = color;
        this.duration = duration;
    }

    /**
     * Gets the target color of the fade.
     *
     * @return the target color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Gets the duration of the fade in milliseconds.
     *
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Gets whether or not this fade is a snap, i.e. if the duration is zero.
     *
     * @return {@code true} if the duration is zero, {@code false} otherwise
     */
    public boolean isSnap() {
        return duration == 0;
    }
}
